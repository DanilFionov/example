package animals;

public class Kotik {
    private String name;
    private String voice;
    private int satiety;
    private int weight;
    private static int count;
    private static final int METHODS = 5;

    /* METHODS */
    public void eatAndAddition (int day , String[] list , int satiety){
        eat(satiety);
        list[day] = day + " - кушает";
    }

    public String[] liveAnotherDay() {
        String[] day = new String[24];
        for (int i = 0; i < 24 ; i++) {
            ;
            switch ((int) (Math.random() * METHODS) + 1) {
                case 1:
                    if (!play()){
                        eatAndAddition(i,day,5);
                        break;
                    }
                    day[i] = i + " - играет";
                    break;
                case 2:
                    if (!sleep()){
                        eatAndAddition(i,day,5);
                        break;
                    }
                    day[i] = i + " - спит";
                    break;
                case 3:
                    if (!wash()){
                        eatAndAddition(i,day,5);
                        break;
                    }
                    day[i] = i + " - умывается";
                    break;
                case 4:
                    if (!walk()){
                        eatAndAddition(i,day,5);
                        break;
                    }
                    day[i] = i + " - гуляет";
                    break;
                case 5:
                    if (!hunt()){
                        eatAndAddition(i,day,5);
                        break;
                    }
                    day[i] = i + " - охотится";
                    break;
            }
        }
        return day;
    }


    public boolean play() {
        if (satiety > 0) {
            satiety--;
            return true;
        } else {
            return false;
        }
    }

    public boolean sleep() {
        if (satiety > 0) {
            satiety--;
            return true;
        } else {
            return false;
        }
    }

    public boolean wash() {
        if (satiety > 0) {
            satiety--;
            return true;
        } else {
            return false;
        }
    }

    public boolean walk() {
        if (satiety > 0) {
            satiety--;
            return true;
        } else {
            return false;
        }
    }

    public boolean hunt() {
        if (satiety > 0) {
            satiety--;
            return true;
        } else {
            return false;
        }
    }

    public void eat(int satiety) {
        this.satiety += satiety;
    }

    public void eat(int satiety, String food) {

    }

    public void eat() {
        eat(satiety);
    }

    /* CONSTRUCTION */
    public Kotik(String name, String voice, int satiety, int weight) {
        this();
        this.name = name;
        this.voice = voice;
        this.satiety = satiety;
        this.weight = weight;
    }

    public Kotik() {
        count++;
    }

    /**
     * Getters and Setters
     **/
    public static int getCount() {
        return count;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public String getVoice() {
        return voice;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public int getSatiety() {
        return satiety;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

}
