import animals.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik first = new Kotik("kitty","myu",10,2000);
        Kotik two = new Kotik();
        two.setName("barsik");
        two.setVoice("miiiiooooouuuuuu");
        two.setSatiety(8);
        two.setWeight(3000);

        for (String str: first.liveAnotherDay()) {
            System.out.println(str);
        }
        compareVoice(first, two);

        System.out.println("Имя коти: " + first.getName() + ", вес коти: " + first.getWeight() + " грамм");
        System.out.println("Колличество котеек: " + Kotik.getCount());
    }
    public static boolean compareVoice(Kotik one, Kotik two){
        boolean mark = false;
        if (one.getVoice().equals(two.getVoice())){
            mark = true;
            System.out.println("Голоса одинаковые");
        }
        else{
            System.out.println("Голоса разные");
            return mark;
        }
        return mark;
    }
}
